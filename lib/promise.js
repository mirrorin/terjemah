'use strict'

var chalk = require('chalk')
var bluebird;

if (typeof Promise === 'undefined') {
  try {
    bluebird = require('bluebird');
  } catch (e) {
    console.log();
    console.log(' Your Node.JS is %s.', chalk.magenta('to old'));
    console.log(' Please run the following command to support Promise.');
    console.log();
    console.log(' $ %s -g %s', chalk.cyan('npm install'), chalk.blue('bluebird'));
    console.log();
    throw e;
  }

  module.exports = bluebird;
} else {
  module.exports = Promise;
}
