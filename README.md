Terjemah
======================
## Sekilas
```bash
Program Penterjemah dari Inggris ke Indonesia, atau dari manapun ke manapun, karena menggunakan
Google Translate. Secara default menggunakan auto mode bahasa Inggris.
```


## Install
```bash
Masih belum PD buat di post ke npmjs :v
```

## Cara Penggunaan

```bash
$ terjemah --to=id Saya

atau

$ terjemah --from=id --to=ja Saya

Karena secara default saya set Bahasanya "Auto-EN" 
```

### Output Format JSON

```bash
$ terjemah --to=id --json --perty Me
```

## Lisensi

```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2012 Romain Lespinasse <romain.lespinasse@gmail.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
```
